package ua.dp.krotov.gwtUiBinderLogin.server.model;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

/**
 * Created by Евгений on 06.01.2016.
 */
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private long id;

    @NaturalId
    @Column(name = "LOGIN", nullable = false)
    private String login;

    @NaturalId
    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @NaturalId
    @Column(name = "NAME", nullable = false)
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
