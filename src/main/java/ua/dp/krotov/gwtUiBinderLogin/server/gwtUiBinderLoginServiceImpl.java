package ua.dp.krotov.gwtUiBinderLogin.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import ua.dp.krotov.gwtUiBinderLogin.client.gwtUiBinderLoginService;

public class gwtUiBinderLoginServiceImpl extends RemoteServiceServlet implements gwtUiBinderLoginService {
    // Implementation of sample interface method
    public String getMessage(String msg) {
        return "Client said: \"" + msg + "\"<br>Server answered: \"Hi!\"";
    }
}