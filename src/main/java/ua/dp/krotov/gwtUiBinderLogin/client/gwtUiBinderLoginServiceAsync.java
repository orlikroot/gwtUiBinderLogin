package ua.dp.krotov.gwtUiBinderLogin.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface gwtUiBinderLoginServiceAsync {
    void getMessage(String msg, AsyncCallback<String> async);
}
