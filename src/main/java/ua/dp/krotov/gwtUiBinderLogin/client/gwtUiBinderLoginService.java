package ua.dp.krotov.gwtUiBinderLogin.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("gwtUiBinderLoginService")
public interface gwtUiBinderLoginService extends RemoteService {
    // Sample interface method of remote interface
    String getMessage(String msg);

    /**
     * Utility/Convenience class.
     * Use gwtUiBinderLoginService.App.getInstance() to access static instance of gwtUiBinderLoginServiceAsync
     */
    public static class App {
        private static gwtUiBinderLoginServiceAsync ourInstance = GWT.create(gwtUiBinderLoginService.class);

        public static synchronized gwtUiBinderLoginServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
